create sequence seq_indicator increment by 10 owned by indicator.id;
create sequence seq_country increment by 10 owned by country.id;
create sequence seq_country_indicator increment by 10 owned by country_indicator.id;

alter table indicator alter column id set default nextval('data.seq_indicator');
alter table country alter column id set default nextval('data.seq_country');
alter table country_indicator alter column id set default nextval('data.seq_country_indicator');

SELECT setval('data.seq_indicator', (select max(id) from indicator));
SELECT setval('data.seq_country', (select max(id) from country));
SELECT setval('data.seq_country_indicator', (select max(id) from country_indicator));
