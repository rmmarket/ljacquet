alter table indicator add constraint ch_indicator_time_start check (time_start >= '01.01.1972');
alter table indicator add constraint ch_indicator_time_end check (time_end <= '01.01.9999');
alter table country add constraint ch_country_time_start check (time_start >= '01.01.1972');
alter table country add constraint ch_country_time_end check (time_end <= '01.01.9999');
alter table country_indicator add constraint ch_country_indicator_value check (value >= 0);
alter table indicator add constraint ch_indicator_unit check (unit = 'human' OR unit = 'percent');
alter table country add constraint ch_country_object_type check (object_type = 'country' OR object_type = 'continent');