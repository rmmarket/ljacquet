create table indicator
(
  id bigint NOT NULL,
  name varchar NOT NULL,
  unit varchar NOT NULL,
  CONSTRAINT pk_indicator PRIMARY KEY (id),
  CONSTRAINT uk_indicator_name UNIQUE (name)
);
comment on table indicator is 'Three human unique indicators during Beta Testing';
comment on column indicator.id is 'Indicator by order';
comment on column indicator.name is 'Indicators name';
comment on column indicator.unit is 'Indicators unit';
create table country
(
  id bigint NOT NULL,
  name varchar NOT NULL,
  par_id bigint,
  object_type varchar NOT NULL DEFAULT 'country',
  CONSTRAINT pk_country PRIMARY KEY (id),
  CONSTRAINT uk_country_name_object_type UNIQUE (name, object_type),
  CONSTRAINT fk_par_id_country FOREIGN KEY (par_id) REFERENCES country(id)
);
comment on table country is 'Continent or Country from https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_population';
comment on column country.id is 'Continent or Country by order';
comment on column country.name is 'Continent or Country name';
comment on column country.par_id is 'Continents id for country, null for continent';
comment on column country.object_type is 'Continent or Country';
create table country_indicator
(
  id bigint NOT NULL,
  c_id bigint NOT NULL,
  i_id bigint NOT NULL,
  value bigint NOT NULL,
  actual_date date NOT NULL,
  CONSTRAINT pk_ci PRIMARY KEY (id)
);
comment on table country_indicator is 'country indicator in time';
comment on column country_indicator.id is 'id';
comment on column country_indicator.c_id is 'Country id';
comment on column country_indicator.i_id is 'Indicators id';
comment on column country_indicator.value is 'value';
comment on column country_indicator.actual_date is 'actual_date';
