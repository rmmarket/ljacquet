insert into country_indicator(id, c_id, i_id, value, actual_date)
select  row_number() over() as id,
        id as c_id,
        (select id from indicator where name = 'Population of country') as i_id,
        (random()*1000000)::integer as value,
        ('2019-'||lpad(i::varchar,2,'0')||'-01')::date as actual_date
from country,
generate_series(1,12) as gs(i)
where object_type = 'country';

insert into country_indicator(id, c_id, i_id, value, actual_date)
select  (row_number() over()) + (select max(id) from country_indicator) as id,
        id as c_id,
        (select id from indicator where name = 'Unemployment rate') as i_id,
        (random()*100)::integer as value,
        ('2019-'||lpad(i::varchar,2,'0')||'-01')::date as actual_date
from country,
generate_series(1,12) as gs(i)
where object_type = 'country';

insert into country_indicator(id, c_id, i_id, value, actual_date)
select  (row_number() over()) + (select max(id) from country_indicator) as id,
        id as c_id,
        (select id from indicator where name = 'Infected humans COVID-19') as i_id,
        (random()*50)::integer as value,
        i as actual_date
from country,
generate_series('2020-05-09', '2020-08-31', '1 day'::interval) as gs(i)
where object_type = 'country';
