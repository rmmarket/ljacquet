alter table indicator add column time_start timestamp default '01.01.1972' not null;
comment on column indicator.time_start is 'time_start';
alter table indicator add column time_end timestamp default '01.01.9999' not null;
comment on column indicator.time_end is 'time_end';
alter table country add column time_start timestamp default '01.01.1972' not null;
comment on column country.time_start is 'time_start';
alter table country add column time_end timestamp default '01.01.9999' not null;
comment on column country.time_end is 'time_end';
